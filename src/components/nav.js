import React, { useState, useContext } from 'react';
import { Link } from 'gatsby';
import './nav.css';



function Navb() {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (

    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
            Ben Perlman
            <i className='fab fa-typo3' />
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link
                to='/resume/'
                className='nav-links'
                onClick={closeMobileMenu}
                reloadDocument
              >
                Resume
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                to='/projects/'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Projects
              </Link>
            </li>
         
          </ul>


        </div>
      </nav>
    </>
  );
}

export default Navb;
