import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import Navbar from './navbar'
import Navb  from './nav'


const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `#367588      `,
      marginBottom: `1rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `2 1.0875rem`,
      }}
    >
      {/* <h2 style={{ margin: 0 }}> */}
        {/* <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link> */}

        {/* <Link to="/resume/"
                    className="page-link"
                    style={{
                        boxShadow: `none`,
                        textDecoration: `none`,
                        color: `white`,
                    }}
                >
                </Link>
          <Link to="/projects/"
                    className="page-link"
                    style={{
                        boxShadow: `none`,
                        textDecoration: `none`,
                        color: `white`,
                    }}
                >
                </Link>                 */}


        <Navbar></Navbar>
      {/* </h2> */}
   

    </div>

  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
